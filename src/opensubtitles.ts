import axios, { AxiosResponse } from "axios";
import fs from "fs";
import sprintf from "sprintf";
import { OPEN_SUBTITLES_USER_AGENT } from "./env";
import { Stream } from "stream";
import zlib from "zlib";

export interface ISubtitleResult {
    MovieByteSize: string;
    MovieFPS: string;
    SubDownloadLink: string;
    SubFileName: string;
    Score: number;
}

export function findSubtitles(
    imdbId: string,
    languageCode: string,
): Promise<AxiosResponse<ISubtitleResult[]>> {
    const formattedImdbId = sprintf.sprintf("%07d", Number(imdbId.replace("tt", "")));

    return axios.get(
        `https://rest.opensubtitles.org/search/imdbid-${formattedImdbId}/sublanguageid-${languageCode}`,
        {
            headers: {
                "X-User-Agent": OPEN_SUBTITLES_USER_AGENT,
            },
        },
    );
}

export async function downloadSubtitleFile(
    url: string,
    subFilename: string,
    movieFilename: string,
): Promise<string> {
    const { data } = await axios.get(url, {
        headers: {
            "X-User-Agent": OPEN_SUBTITLES_USER_AGENT,
        },
        responseType: "stream",
    });

    const subFilenameExtension = subFilename.substr(subFilename.lastIndexOf("."));
    const outputFilename =
        movieFilename.substr(0, movieFilename.lastIndexOf(".")) + subFilenameExtension;

    const stream: Stream = data
        .pipe(zlib.createGunzip())
        .pipe(fs.createWriteStream(outputFilename));

    return new Promise((resolve, reject) => {
        stream.on("finish", err => {
            if (err != null) {
                reject(err);
                return;
            }

            resolve(outputFilename);
        });
    });
}
