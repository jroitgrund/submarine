import fs from "fs";
import path from "path";
import parse from "csv-parse";
import _ from "lodash";

async function getLanguageCodeMap(): Promise<Array<[string, string]>> {
    return new Promise((resolve, reject) => {
        fs.readFile(path.join(__dirname, "../files/codes-map.csv"), "utf8", function(
            err,
            contents,
        ) {
            if (err != null) {
                reject(err);
                return;
            }

            parse(contents, function(err, output) {
                if (err != null) {
                    reject(err);
                    return;
                }

                resolve(output);
            });
        });
    });
}

const languageCodeMap = getLanguageCodeMap();

export async function findLanguageCode(language: string | undefined) {
    if (language == null) {
        return "eng";
    }
    const map = await languageCodeMap;
    const lowerLanguage = language.toLowerCase();
    const entry = _.find(map, ([_code, name]) => name.toLowerCase().includes(lowerLanguage));
    return entry && entry[0];
}
