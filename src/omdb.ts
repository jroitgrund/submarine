import axios, { AxiosResponse } from "axios";
import { OMDB_API_KEY } from "./env";

export interface IMatchingMovieSearch {
    Search: IMatchingMovie[];
}

export interface IMatchingMovie {
    Title: string;
    Year: string;
    imdbID: string;
}

export function getMatchingMovies(title: string): Promise<AxiosResponse<IMatchingMovieSearch>> {
    return axios.get(`http://www.omdbapi.com/?apikey=${OMDB_API_KEY}&s=${title}`);
}
