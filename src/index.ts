#!/usr/bin/env node

import "source-map-support";
import _ from "lodash";
import "./env";
import { getFramerateAndSize } from "./framerate";
import { getInput } from "./input";
import { findLanguageCode } from "./languages";
import { getMatchingMovies, IMatchingMovie } from "./omdb";
import { findSubtitles, ISubtitleResult, downloadSubtitleFile } from "./opensubtitles";
import { findMissingDep } from "./check-deps";

interface IResultPredicate {
    matches: (subResult: ISubtitleResult) => boolean;
    name: string;
}

(async () => {
    try {
        const [title, filename, language] = process.argv.slice(2);

        if (title == null || filename == null) {
            console.error("Usage: captainc title filename [language=english]");
            console.error("e.g.");
            console.error('$ captainc "after hours" after-hours.mp4');
            console.error('$ captainc "stranger than paradise" stranger.mp4 esperanto');
            return;
        }

        console.error(`Searching for ${title}...`);

        const missingDep = await findMissingDep();

        if (missingDep != null) {
            console.error(
                `captain-caption requires ${missingDep}. Please install it and try again.`,
            );
            return;
        }

        const fileInfoPromise = getFramerateAndSize(filename);
        const codePromise = findLanguageCode(language);
        const movieResultsPromise = getMatchingMovies(title);

        const languageCode = await codePromise;
        if (languageCode == null) {
            console.error(`Couldn't find language ${language}`);
            return;
        }
        console.error(`Language: ${language}`);

        const movieResults = (await movieResultsPromise).data.Search;
        if (movieResults.length == 0) {
            console.error("No results.");
            return;
        }

        const imdbID = await getImdbId(movieResults);
        const subResults = _.sortBy(
            (await findSubtitles(imdbID, languageCode)).data,
            result => result.Score,
        );

        const { framerate, size } = await fileInfoPromise;

        console.error("Framerate: ", framerate);
        console.error("Bytesize: ", size);

        const predicates: IResultPredicate[] = [
            {
                matches: ({ MovieByteSize }) => Number(MovieByteSize) == Number(size),
                name: "matching byte size",
            },
            {
                matches: ({ MovieFPS }) => Number(framerate) == Number(MovieFPS),
                name: "matching frames per second",
            },
            { matches: () => true, name: "highest opensubtitles.org score" },
        ];

        let bestResult: ISubtitleResult | undefined;
        let predicate: IResultPredicate | undefined;
        while (bestResult == null && (predicate = predicates.shift())) {
            bestResult = _.find(subResults, predicate.matches);
        }

        if (bestResult == null || predicate == null) {
            throw new Error();
        }

        console.error(`Found best subtitles by ${predicate.name}`);

        const { SubDownloadLink, SubFileName } = bestResult;

        const outputFilename = await downloadSubtitleFile(SubDownloadLink, SubFileName, filename);

        console.error(`Subtitles downloaded to ${outputFilename}`);
    } catch (e) {
        console.error("Error occured", e);
    }
})();

async function getImdbId(movieResults: IMatchingMovie[]) {
    if (movieResults.length == 1) {
        const { imdbID, Title, Year } = movieResults[0];
        console.error(`Found "${Title} (${Year})".`);
        return imdbID;
    }

    console.error("Choose the correct movie result.");
    movieResults.forEach(({ Title, Year }, i) => {
        console.error(`${i + 1}. ${Title} (${Year})`);
    });

    const { imdbID } = movieResults[Number(await getInput("> ")) - 1];

    return imdbID;
}
