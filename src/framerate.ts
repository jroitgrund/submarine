import { exec } from "child_process";
import fs from "fs";

const regex = /([\d+\.]+)\sfps/;

export async function getFramerateAndSize(filename: string) {
    const frameratePromise = new Promise((resolve, reject) => {
        exec(`ffprobe -i "${filename}"`, (err, _$, stderr) => {
            if (err != null) {
                reject(err);
                return;
            }

            const match = stderr.match(regex);

            if (match == null || match[1] == null) {
                reject(new Error("Couldn't determine video FPS"));
                return;
            }

            resolve(match[1]);
        });
    });

    const sizePromise = new Promise((resolve, reject) => {
        fs.stat(filename, (err, stats) => {
            if (err != null) {
                reject(err);
                return;
            }

            resolve(stats.size);
        });
    });

    const [framerate, size] = await Promise.all([frameratePromise, sizePromise]);

    return {
        framerate,
        size,
    };
}
