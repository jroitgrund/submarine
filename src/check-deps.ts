import { exec } from "child_process";

export function findMissingDep(): Promise<string | undefined> {
    return new Promise(resolve => {
        ["ffprobe"].forEach(command => {
            exec(`/bin/sh -c "which ${command}"`, err => {
                if (err != null) {
                    resolve(command);
                }

                resolve(undefined);
            });
        });
    });
}
