import * as dotenv from "dotenv";
dotenv.config();

export const OMDB_API_KEY = process.env["OMDB_API_KEY"];
export const OPEN_SUBTITLES_USER_AGENT = process.env["OPEN_SUBTITLES_USER_AGENT"];
